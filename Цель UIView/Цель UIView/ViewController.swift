//
//  ViewController.swift
//  Цель UIView
//
//  Created by Anton Zavgorodniy on 4/18/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	let x = 20
	let y = 20
	let size = 250
	let colorLight: UIColor = .white
	let colorDark: UIColor = .black
	let level = 11
	let radius = 100
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		targetSquare(x, y, size, colorLight, colorDark, level)
	}
	
	func drawBox(_ x: Int, _ y: Int, _ size: Int, _ color: UIColor) -> Void{
		let box = UIView()
		box.frame.size.width = CGFloat(size)
		box.frame.size.height = CGFloat(size)
		box.frame.origin.x = CGFloat(x)
		box.frame.origin.y = CGFloat(y)
		box.backgroundColor = color // randomColor() //.cyan
		box.layer.cornerRadius = CGFloat(size / 2)
		view.addSubview(box)
	}
	
	// строим круглую мишень
	func targetSquare(_ x: Int, _ y: Int, _ size: Int, _ colorLight: UIColor, _ colorDark: UIColor, _ level: Int) {
		var x = x
		var y = y
		let countRing = level
		var color = colorDark
		var sizeRing = size
		let step = sizeRing / countRing
		for i in 0...countRing {
			color = (i % 2 == 0) ? colorLight : colorDark
			drawBox(x, y, sizeRing, color)
			sizeRing = sizeRing - step
			x += step / 2
			y += step / 2
		}
	}
}

