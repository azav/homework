//
//  ViewController.swift
//  HomeWork_2_2
//
//  Created by Anton Zavgorodniy on 4/9/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		print("Задача 1")
		print()
		let startCoast = 24.0
		let comBank = 0.06
		let startYear = 1826
		let endYear = 2019
		let priceNow = howMuchMoneyTheSettlers(startPrice: startCoast, fee: comBank, dateDepositStart: startYear, dateDepositStop: endYear)
		print("Cумма составляет \(Int(priceNow))$")
		print()
		
		print("Задача 2")
		print()
		let monthlyScholarship = 700.0
		let livingExpenses = 1000.0
		let investment = howMuchMoneyNeedStudentForLiveTenMonths(monthlyScholarship: monthlyScholarship, livingExpenses: livingExpenses)
		print("Необходимо инвестиций \(investment) грн")
		print()
		
		print("Задача 3")
		print()
		let savingMoney = 2400.00
		let howMonths = howManyMonthsWillTheStudentLive(savingMoney: savingMoney)
		print("студент прожил \(howMonths) месяцев спокойно, а потом умер :(")
		print()
		
		print("Задача 4")
		print()
		let number = Int.random(in: 10..<100)
		print("Число - \(number)")
		let reversNumber = inversNumberFromRandom(number: number)
		print("Число задом на перед - \(reversNumber)")
		
	}
	/*	Задача 1. Остров Манхэттен был приобретен поселенцами за $24 в 1826 г. Каково было бы в настоящее время состояние их счета, если бы эти 24 доллара были помещены тогда в банк под 6% годового дохода? */
	func howMuchMoneyTheSettlers(startPrice: Double, fee: Double, dateDepositStart: Int, dateDepositStop: Int) -> Double {
		var price = startPrice
		for _ in (dateDepositStart + 1)...dateDepositStop {
			price = price * fee + price
		}
		return price
	}
	
	/*	Задача 2. Ежемесячная стипендия студента составляет 700 гривен, а расходы на проживание превышают ее и составляют 1000 грн. в месяц. Рост цен ежемесячно увеличивает расходы на 3%. Определить, какую нужно иметь сумму денег, чтобы прожить учебный год (10 месяцев), используя только эти деньги и стипендию. */
	func howMuchMoneyNeedStudentForLiveTenMonths(monthlyScholarship: Double, livingExpenses: Double) -> Double {
		var totaLlivingExpenses = livingExpenses
		let costOfLivingPerMonth = 0.03
		var totalBaseExpensis = 0.0
		var totalBaseIncome = 0.0
		var needInvestment = 0.0
		for i in 1...10 {
			if i == 1 { //расчет первого месяца
				totalBaseIncome = monthlyScholarship
				totalBaseExpensis = livingExpenses
			}
			else { //расчет оставшихся месяцев
				totalBaseIncome = totalBaseIncome + monthlyScholarship
				totaLlivingExpenses = totaLlivingExpenses + (totaLlivingExpenses * costOfLivingPerMonth)
				totalBaseExpensis = totalBaseExpensis + livingExpenses
			}
		}
		needInvestment = Double(round(100 * (totalBaseExpensis - totalBaseIncome)) / 100) //вычисляем сколько нехватает
		return needInvestment
	}
	
	/*	Задача 3. У студента имеются накопления 2400 грн. Ежемесячная стипендия составляет 700 гривен, а расходы на проживание превышают ее и составляют 1000 грн. в месяц. Рост цен ежемесячно увеличивает расходы на 3%. Определить, сколько месяцев сможет прожить студент, используя только накопления и стипендию. */
	func howManyMonthsWillTheStudentLive(savingMoney: Double) -> Int {
		let savingMoney = 2400.00
		let scholarshipMonthly = 700.0
		var expensesLiving = 1000.0
		var totalExpensive = 0.0
		var totalIncome = 0.0
		let percentageIncrease = 0.03
		var months = 0
		while (totalExpensive <= totalIncome) {
			months += 1
			if months == 1 { //считаем первый месяц
				totalExpensive = expensesLiving
				totalIncome = savingMoney + scholarshipMonthly
			}
			else { //считаем остальные месяцы
				totalIncome = totalIncome + scholarshipMonthly
				expensesLiving = expensesLiving + (expensesLiving * percentageIncrease)
				totalExpensive = totalExpensive + expensesLiving
			}
			if totalIncome <= totalExpensive {
				months -= 1 //возвращаемся на месяц назад
			}
		}
		return months
	}
	
	/*	Задача 4. 2хзначную цело численную переменную типа 25, 41, 12. После выполнения вашей программы у вас в другой переменной должно лежать это же число только задом на перед 52, 14, 21 */
	func inversNumberFromRandom(number: Int) -> Int {
		var inputNumber = number
		var inversNumber = 0
		while inputNumber > 0 {
			inversNumber = inversNumber * 10 + inputNumber % 10
			inputNumber = inputNumber / 10
		}
		return inversNumber
	}
	
	
}

