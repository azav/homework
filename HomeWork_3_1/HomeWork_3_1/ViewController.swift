//
//  ViewController.swift
//  HomeWork_3_1
//
//  Created by Anton Zavgorodniy on 4/12/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		print("Задача 1")
		let name = "Антон"
		print("В моем имени \(nameCount(name: name)) букв")
		print()
		
		print("Задача 2")
		let midName = "Игоревич"
		print(sufixMidName(midName: midName))
		print()
		
		print("Задача 3")
		let fulName = "AntonZavgorodniy"
		print("Первая строка - \(onlyName(string: sumWords(string: fulName)))")
		print("Вторая строка - \(onlyLastName(string: sumWords(string: fulName)))")
		print("Третья строка - \(sumWords(string: fulName))")
		print()
		
		print("Задача 4")
		let stringForReverse = "А роза упала на лапу Азора"
		print("Заданная строка - \(stringForReverse)")
		print("Перевернутая строка - \(reverse(string: stringForReverse))")
		print()
		
		print("Задача 5")
		let numberForComma = "1234567890"
		print("Число без разделителей - \(numberForComma)")
		print("Число с разделителями - \(addComma(number: numberForComma))")
		print()
		
		print("Задача 6")
		let password = "1Aa$password"
		let levelOfYourPassword = getSecurityLevel(password)
		if levelOfYourPassword < 5 {
			print("Увы, уровень надежности вашего пароля всего лишь - \(levelOfYourPassword) из 5")
		}
		else {
			print("Поздравляем! Уровень надежности вашего пароля максимальный и равен - \(levelOfYourPassword)")
		}
		print()
		
		print("Задача 7")
		let rawArray = [9, 1, 2, 5, 1, 7]
		print("Результирующий массив - \(sortAscendingAndDelete(rawArray))")
		print()
		
		print("Задача 8")
		let stringForTranslate = "ЯЗзЬ"
		print("Транслитерированная строка - \(translit(stringForTranslate))")
		print()
		
		print("Задача 9")
		let stringArray = ["lada", "sedan", "baklazhan"]
		print(searchCharsInArray(stringArray, chars: "da"))
		print()
		
		print("Задача 10")
		let matString = "hello my fuck"
		print("Слово без матов - \(removeMatFromString(matString))")
		print()
	}
	// Задача 1 создать строку с своим именемвывести количество символов содержащихся в ней
	func nameCount(name: String) -> Int {
		let count = name.count
		return count
	}
	
	// Задача 2 создать строку с своим отчеством проверить его на окончание “ич/на”(в классе написан метод который позволяет проверить на суффикс или префикс, найдите и используйте его)
	func sufixMidName(midName: String) -> String {
		var suffix = ""
		if midName.lowercased().hasSuffix("ич") {
			suffix = "В вашем имени есть суфикс -=ИЧ=-"
		} else if midName.lowercased().hasSuffix("на") {
			suffix = "В вашем имени есть суфикс -=НА=-"
		} else {
			suffix = "В вашем отчестве нет суфиксов -=ИЧ/НА=-"
		}
		return suffix
	}
	
	// Задача 3. Cоздать строку, где слитно написано Ваши ИмяФамилия “IvanVasilevich"
	//	Вам нужно разбить на две отдельных строки из предыдущей создать третью, где они обе будут разделены пробелом
	//	str1 = “Ivan”
	//	str2 = “Vasilevich”
	//	str3 = “Ivan Vasilevich"
	func sumWords(string: String) -> String {
		var result = ""
		for character in string {
			if String(character) == String(character).uppercased() {
				result.append(" ")
			}
			result.append(character)
		}
		return result
	}
	func onlyName(string: String) -> String {
		let index = string.lastIndex(of: " ") ?? string.endIndex
		let beginning = string[..<index]
		let name = String(beginning)
		return name
	}
	func onlyLastName(string: String) -> String {
		let index = string.lastIndex(of: " ") ?? string.endIndex
		let beginning = string[index...]
		let LastName = String(beginning)
		return LastName
	}
	
	// Задача 4. Вывести строку зеркально Ось → ьсО не используя reverse (посимвольно)
	func reverse(string: String) -> String {
		var result = ""
		for character in string {
			let revString = "\(character)"
			result = revString + result
		}
		return result
	}
	
	//	Задача 5. Добавить запятые в строку как их расставляет калькулятор
	//	1234567 → 1,234,567
	//	12345 → 12,345
	//	(не использовать встроенный метод для применения формата)
	func addComma(number: String) -> String {
		var result = ""
		for (index, element) in number.reversed().enumerated() {
			result.insert(element, at: result.startIndex)
			if((index + 1) % 3 == 0 && index != number.count - 1) {
				result.insert(",", at: result.startIndex)
			}
		}
		return result
	}
	
	//	Задача 6. Проверить пароль на надежность от 1 до 5
	//	a) если пароль содержит числа +1
	//	b) символы верхнего регистра +1
	//	c) символы нижнего регистра +1
	//	d) спец символы +1
	//	e) если содержит все вышесказанное
	//	Пример:
	//	123456 - 1 a)
	//	qwertyui - 1 c)
	//	12345qwerty - 2 a) c)
	//	32556reWDr - 3 a) b) c)
	func getSecurityLevel(_ password: String) -> Int{
		var security = 0
		
		if includesNumber(password) {
			security += 1
		}
		if  includesUpercaseChar(password) {
			security += 1
		}
		if  includesLowcaseChar(password) {
			security += 1
		}
		if  includesSpecialChar(password) {
			security += 1
		}
		if includesNumber(password) && includesUpercaseChar(password) && includesLowcaseChar(password) && includesSpecialChar(password) {
			security += 1
		}
		return security
	}
	//Проверка содержания чисел в пароле
	func includesNumber(_ string: String) -> Bool {
		let numbersRange = string.rangeOfCharacter(from: .decimalDigits)
		return (numbersRange != nil)
	}
	//Проверка содержания символов верхнего регистра в пароле
	func includesUpercaseChar(_ string: String) -> Bool {
		return string.rangeOfCharacter(from: CharacterSet.uppercaseLetters) != nil
	}
	//Проверка содержания символов нижнего регистра в пароле
	func includesLowcaseChar(_ string: String) -> Bool {
		return string.rangeOfCharacter(from: CharacterSet.lowercaseLetters) != nil
	}
	//Проверка содержания спец символов в пароле
	func includesSpecialChar(_ string: String) -> Bool {
		return string.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) != nil
	}
	
	//	Задача 7. Сортировка массива не встроенным методом по возрастанию + удалить дубликаты
	//	[9, 1, 2, 5, 1, 7]
	func sortAscendingAndDelete(_ array: [Int]) -> [Int] {
		let sortArray = sortAscendingArray(array)
		let sortAndDeleteArray = deleteDuplicatesInArray(sortArray)
		return sortAndDeleteArray
	}
	// Сортировка массива
	func sortAscendingArray(_ array: [Int]) -> [Int] {
		var result = array
		for i in 1..<result.count {
			var y = i
			while y > 0 && result[y] < result[y - 1] {
				result.swapAt(y - 1, y)
				y -= 1
			}
		}
		return result
	}
	//Удаление дубликатов в отсортированном массиве
	func deleteDuplicatesInArray(_ array: [Int]) -> [Int] {
		let sortArray = Array(Set(array))
		return sortArray
	}
	
	//	Задача 8. Написать метод, который будет переводить строку в транслит.
	//	Пример:
	//	print(convertStrToTranslite:”ЯЗЗЬ”) → “YAZZ”
	//	print(convertStrToTranslite:”морДа”) → “morDa”
	func translit(_ string: String) -> String {
		let transLit = ["А":"A", "а":"a", "Б":"B", "б":"b", "В":"V", "в":"v", "Г":"G", "г":"g", "Д":"D", "д":"d", "Е":"E", "е":"e", "Ж":"ZH", "ж":"zh", "З":"Z", "з":"z", "И":"I", "и":"i", "Й":"J", "й":"j", "К":"K", "к":"k", "Л":"L", "л":"l", "М":"M", "м":"m", "Н":"N", "н":"n", "О":"O", "о":"o", "П":"P", "п":"p", "Р":"R", "р":"r", "С":"S", "с":"s", "Т":"T", "т":"t", "У":"U", "у":"u", "Ф":"F", "ф":"f", "Х":"H", "х":"h", "Ц":"C", "ц":"c", "Ч":"CH", "ч":"ch", "Ш":"SH", "ш":"sh", "Щ":"SHCH", "щ":"shch", "Ь":"'", "ь":"'", "Э":"EH", "э":"eh", "Ю":"YU", "ю":"yu", "Я":"YA", "я":"ya"]
		var result = ""
		for character in string {
			var transLitString = ""
			transLitString = transLit["\(character)"]!
			result += transLitString
		}
		return result
	}
	
	//	Задача 9. Сделать выборку из массива строк в которых содержится указанная строка
	//	[“lada”, “sedan”, “baklazhan”] search “da”
	//	→ [“lada”, “sedan”] - sort() && sort using NSPredicate + manual (for loop)
	func searchCharsInArray(_ array: [String], chars searchChars: String) -> [String] {
		let result = array.filter { $0.contains(searchChars) }
		return result
	}
	
	//	Задача 10. Set<String> - antimat [“fuck”, “fak”] “hello my fak” “hello my ***”
	//	использовать Set или NSSet для программы antimat - исключить из предложения все слова содержащиеся в сете
	func removeMatFromString (_ stringForRemoveMat: String) -> String {
		let string = stringForRemoveMat
		let dicMat: Set<String> = ["fuck", "fak"]
		let stars = "*"
		var result = ""
		var matMask = ""
		for word in dicMat {
			if string.contains(word) {
				for _ in 1...word.count {
					matMask += stars
				}
				result = string.replacingOccurrences(of: word, with: matMask)
			}
		}
		return result
	}
}

