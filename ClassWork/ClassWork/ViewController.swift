//
//  ViewController.swift
//  ClassWork
//
//  Created by Anton Zavgorodniy on 4/16/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	override func viewDidLoad() -> Void {
		super.viewDidLoad()
		
		hello()
		whichDay(8)
		sumTwoNumbers(firstNumber: 3, secondNumber: 5)
		let numForMath = 4
		print("Число расно - \(numberMultiplyAndSum(number: numForMath))")
		helloManyTimes(4)
		print(fib(6))
		print("Непонятно какая логика - \(fuckinLogic(4))")
		
	}
	// Задача 1. Записать метод которий виведет в консоль сообщение С приветсвием "Привет" (без аргументов)
	func hello() {
		print("Привет!")
		return
	}
	
	// Задача 2. Записать метод которий будет выводить в консоль сообщение какой день недели относительно числа
	// которое попадает в метод как аргумент
	func whichDay (_ numberOfDay: Int) -> Void {
		let day = numberOfDay
		switch day {
		case 1:
			print("Понедельник")
		case 2:
			print("Вторник")
		case 3:
			print("Среда")
		case 4:
			print("Четверг")
		case 5:
			print("Пятница")
		case 6:
			print("Суббота")
		case 7:
			print("Воскресенье")
		default:
			print("Не верный номер дня недели!")
		}
		return
	}
	
	//	Задача 3. Записать метод которий выводит в консоль суму двух чисел
	func sumTwoNumbers(firstNumber: Int, secondNumber: Int) -> Void {
		print("Сумма двух чисел равна \(firstNumber + secondNumber)")
		return
	}
	
	//	Задача 4. Записать метод которий будет умножать задание число на 30 прибавлять 5 и возвращать результат
	//	вичислений, єтот результат нужно использовать в методе viewDidLoad для вивода в консоль
	func numberMultiplyAndSum(number: Int) -> Int {
		var result = 0
		result = (number * 30) + 5
		return result
	}
	
	//	Задача5. Записать метод которий виведет в консоль сообщение С приветсвием "Привет” заданное количество раз
	func helloManyTimes(_ howManyTimes: Int) -> Void {
		for _ in 1...howManyTimes {
			print("Привет!")
		}
	}
	//Задача 6. Записать метод который будет показывать число Фибоначчи например fib(num:6) должно вернуть число 8
	func fib(_ number: Int) -> Int {
		var n0 = 1
		var n1 = 1
		var n2 = 0
		var result = 0
		for _ in 3...number {
			n2 = n0 + n1
			result = n2
			n0 = n1
			n1 = n2
		}
		return result
	}
	
	// Написать метод который принимает число N и показывает сумму первых N нечетных чисел, например oddSum(n:4) = (1
	// + 3 + 5 + 7) должно вернуть число 16
	func fuckinLogic(_ numberN: Int) -> Int {
		var result = 0
		var count = 0
		var stepIndex = 0
		while count < numberN {
			stepIndex += 1
			if stepIndex % 2 != 0 {
				result += stepIndex
				count += 1
			}
		}
		return result
	}
}

