//
//  ViewController.swift
//  HomeWork
//
//  Created by Anton Zavgorodniy on 4/8/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		print("Задача 0")
		print()
		let one = Int.random(in: 1..<10)
		let two = Int.random(in: 1..<10)
		whoNumberBigest(numberOne: one, numberTwo: two)
		print()
		
		print("Задача 1")
		print()
		let num = Int.random(in: 1..<200)
		squareAndCubeOfNumber(number: num)
		print()
		
		print("Задача 2")
		print()
		outputNumberInSequence(numberForSequence: Int.random(in: 1..<10))
		print()
		
		print("Задача 3")
		print()
		outputDivNumbers(numberForDivision: Int.random(in: 1..<1000))
		print()
		
		print("Задача 4")
		print()
		let tasteNumber = Int.random(in: 1..<1000) //проверено для чисел 8128, 496, 28, 6
		let isPerfect = outputPerfectNumber(tasteNumber: tasteNumber)
		if isPerfect {
			print("Число \(tasteNumber) - совершенно")
		} else {
			print("Число \(tasteNumber) - не совершенно как и все мы, люди :-(")
		}
	}
	
	// Задача 0. Вывести на экран наибольшее из двух чисел
	func whoNumberBigest(numberOne: Int, numberTwo: Int) -> Void {
		if numberOne < numberTwo {
			print("Число \(numberTwo)" + " больше чем \(numberOne)")
			return
		}
		else {
			if numberTwo == numberOne {
				print("Введенные числа \(numberOne) и \(numberTwo) равны")
				return
			}
			else {
				print("Число \(numberOne) больше чем \(numberTwo)")
				return
			}
		}
	}
	
	// Задача 1. Вывести на экран квадрат и куб числа
	func squareAndCubeOfNumber(number: Int) -> Void {
		print("Квадрат числа \(number) = \(pow(Decimal(number), 2))")
		print("Куб числа \(number) = \(pow(Decimal(number), 3))")
		return
	}
	
	//	Задача 2. Вывести на экран все числа до заданного и в обратном порядке до 0
	func outputNumberInSequence(numberForSequence: Int) -> Void {
		var numForSequence = numberForSequence
		for i in 0...numberForSequence {
			print(i)
		}
		while numForSequence > 0 {
			print("\(numForSequence)")
			numForSequence -= 1
		}
		return
	}
	
	//	Задача 3. Подсчитать общее количество делителей числа и вывести их
	func outputDivNumbers(numberForDivision: Int) -> Void {
		let numberForDivision = Int.random(in: 1..<1000)
		var countDivision = 0
		for i in 1...(numberForDivision / 2) {
			if numberForDivision % i == 0 {
				countDivision += 1
				print(" \(countDivision). Число \(i) является делителем числа \(numberForDivision)")
			}
		}
		print("Число \(numberForDivision) имеет \(countDivision) делителей")
		return
	}
	
	//	Задача 4. Проверить, является ли заданное число совершенным и найти их (делители)
	func outputPerfectNumber(tasteNumber: Int) -> Bool {
		var isPerfectNumber = false
		var sum = 0
		for i in 1..<tasteNumber {
			if tasteNumber % i == 0 {
				sum += i
			}
		}
		if sum == tasteNumber {
			isPerfectNumber = true
		}
		return isPerfectNumber
	}
	
}

