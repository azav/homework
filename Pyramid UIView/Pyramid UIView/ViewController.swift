//
//  ViewController.swift
//  Pyramid UIView
//
//  Created by Anton Zavgorodniy on 4/18/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	let x = 5
	var y = 20
	let size = 40
	let level = 3
	let skipDown = 10
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		super.touchesBegan(touches, with: event)
		createLine(level, x, y, size, skipDown)
		y = y + 2 * (size + skipDown)
		createStairs(level, x, y, size, skipDown)
		y += 5 * (size + skipDown)
		createPyramid(level, x, y, size, skipDown)
	}
	
	func drawBox(_ x: Int, _ y: Int, _ size: Int) -> Void{
		let box = UIView()
		box.frame.size.width = CGFloat(size)
		box.frame.size.height = CGFloat(size)
		box.frame.origin.x = CGFloat(x)
		box.frame.origin.y = CGFloat(y)
		box.backgroundColor = randomColor() //.cyan
		view.addSubview(box)
	}
	
	// Раномно выбирает цвета
	func randomColor() -> UIColor {
		let random = {CGFloat(arc4random_uniform(255)) / 255.0}
		return UIColor(red: random(), green: random(), blue: random(), alpha: 1)
	}
	
	// Задача 1, сделать линию
	func createLine(_ level: Int, _ x: Int, _ y: Int, _ size: Int, _ skipDown: Int) -> Void {
		var x = x
		for _ in 1...level {
			drawBox(x, y, size)
			x += size + skipDown
		}
		return
	}
	
	// Задача 2, сделать лесенку
	func createStairs(_ level: Int, _ x: Int, _ y: Int, _ size: Int, _ skipDown: Int) -> Void {
		let sequence = 1...level
		var y = y
		for i in sequence {
			for j in sequence {
				if i <= j {
					createLine(i, x, y, size, skipDown)
				}
			}
			y = y + size + skipDown
		}
	}
	
	// Задача 3, сделать пирамиду
	func createPyramid(_ level: Int, _ x: Int, _ y: Int, _ size: Int, _ skipDown: Int) -> Void {
		let sequence = 1...level
		var y = y
		var x = x + ((size + skipDown) * level) / 2
		for i in sequence {
			for j in sequence {
				if i <= j {
					createLine(i, x, y, size, skipDown)
				}
			}
			y = y + size + skipDown
			x = x - (size + skipDown) / 2
		}
	}
}

