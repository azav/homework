//
//  SettingsViewController.swift
//  CoffeMachine
//
//  Created by Anton Zavgorodniy on 4/24/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

	@IBOutlet weak var outForTwo: UITextView!
	override func viewDidLoad() {
        super.viewDidLoad()
    }
	@IBAction func addMilk() {
		outForTwo.text = CoffeMachine.shared.fillUp(.milk, 1.0)
	}
	@IBAction func addCoffe() {
		outForTwo.text = CoffeMachine.shared.fillUp(.coffe, 1.0)
	}
	@IBAction func addSugar() {
		outForTwo.text = CoffeMachine.shared.fillUp(.sugar, 1.0)
	}
	@IBAction func addWater() {
		outForTwo.text = CoffeMachine.shared.fillUp(.water, 1.0)
	}
	@IBAction func countRaw() {
		outForTwo.text = CoffeMachine.shared.reportNeed()
	}
	@IBAction func countSale() {
		outForTwo.text = CoffeMachine.shared.reportSale()
	}
}
