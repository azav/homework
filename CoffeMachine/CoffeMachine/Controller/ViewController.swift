//
//  ViewController.swift
//  CoffeMachine
//
//  Created by Anton Zavgorodniy on 4/18/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var outputForOneScreen: UITextView!
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	@IBAction func createAmericano() {
		outputForOneScreen.text = CoffeMachine.shared.coffe(.americano)
	}
	@IBAction func createEspresso() {
		outputForOneScreen.text = CoffeMachine.shared.coffe(.espresso)
	}
	@IBAction func createCapuchino() {
		outputForOneScreen.text = CoffeMachine.shared.coffe(.capuchino)
	}
	@IBAction func createLate() {
		outputForOneScreen.text = CoffeMachine.shared.coffe(.late)
	}
}

