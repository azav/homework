//
//  CoffeMachine.swift
//  CoffeMachine
//
//  Created by Anton Zavgorodniy on 4/22/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import Foundation
import UIKit

class CoffeMachine: NSObject {
	static var shared  = CoffeMachine()
	private override init() { }
	//объем емкостей кофемашины
	private let coffeBox = 5.0
	private let milkBox = 5.0
	private let sugarBox = 5.0
	private let waterBox = 10.0
	// остатки сырья
	private var coffe = 0.3
	private var milk = 0.5
	private var sugar = 0.3
	private var water = 0.8
	// счетчики количества продаж
	private var americano = 0
	private var espresso = 0
	private var late = 0
	private var capuchino = 0
	
	// Приготовление любого напитка
	private func create(_ type: Coffe,
						needCoffe: Double,
						needMilk: Double,
						needSugar: Double,
						needWater: Double) -> String {
		var result = ""
		if self.coffe < needCoffe {
			result = "Не хватает кофе! Отсалось \(cut(self.coffe)). Нужно добавить"
			return result
		}
		else if self.milk < needMilk {
			result = "Не хватает молока! Осталось \(cut(self.milk)). Нужно добавить"
			return result
		}
		else if self.sugar < needSugar {
			result = "Не хватает сахара! Осталось \(cut(self.milk)) Нужно добавить"
			return result
		}
		else if self.water < needWater {
			result = "Не хватает воды! Осталось \(cut(self.water)) Нужно добавить"
			return result
		}
		self.coffe -= needCoffe
		self.water -= needWater
		self.sugar -= needSugar
		self.milk -= needMilk
		type == .americano ? self.americano += 1 : ()
		type == .capuchino ? self.capuchino += 1 : ()
		type == .espresso ? self.espresso += 1 : ()
		type == .late ? self.late += 1 : ()
		result = "Кофе \(type) готов! Приятного аппетита:)"
		return result
	}
	
	// Отчет по остаткам
	func reportNeed() -> String {
		let result = """
		кофе: \(cut(self.coffe)) кг,
		молоко: \(cut(self.milk)) л,
		сахар: \(cut(self.sugar)) кг,
		вода: \(cut(self.water)) л
		"""
		return result
	}
	
	//Отчет по продажам
	func reportSale() -> String {
		let result = """
		Продано:
		Американо: \(self.americano) чашек,
		Эспрессо: \(self.espresso) чашек,
		Лате: \(self.late) чашек,
		Капучино: \(self.capuchino) чашек
		"""
		return result
	}
	
	// Пополнение сырёем кофемашину
	func fillUp(_ type: RawStuff, _ weight: Double) -> String {
		var result = ""
		switch type {
		case .sugar:
			weight + self.sugar < sugarBox ? self.sugar += weight : (result = "Сахар не влазит :-( в коробочку влезет только \(cut(sugarBox - self.sugar)) сахара")
			result = "Сахар добавлен, сахара: \(cut(self.sugar)) кг"
		case .coffe:
			weight + self.coffe <= coffeBox ? self.coffe += weight : (result = "Кофе не влазит :-( в коробочку влезет только \(cut(coffeBox - self.coffe)) кофе")
			result = "Кофе добавлен, кофе: \(cut(self.coffe)) кг"
		case .milk:
			weight + self.milk <= milkBox ? self.milk += weight : (result = "Молоко не влазит :-( в коробочку влезет только \(cut(milkBox - self.milk)) молока")
			result = "Молоко добавлено, молока: \(cut(self.milk)) л"
		case .water:
			weight + self.water <= waterBox ? self.water += weight : (result = "Вода не влазит :-( в аппарат влезет только  \(cut(waterBox - self.water))")
			result = "Вода добавлена, воды: \(cut(self.water)) л"
		}
		return result
	}
	
	// Готовим напитки
	func coffe(_ name: Coffe) -> String {
		var result = ""
		switch name {
		case .americano:
			result = create(.americano,
							needCoffe: 0.05,
							needMilk: 0.0,
							needSugar: 0.02,
							needWater: 0.2)
		case .espresso:
			result = create(.espresso,
							needCoffe: 0.05,
							needMilk: 0.0,
							needSugar: 0.02,
							needWater: 0.1)
		case .capuchino:
			result = create(.capuchino,
							needCoffe: 0.05,
							needMilk: 0.1,
							needSugar: 0.03,
							needWater: 0.1)
		case .late:
			result = create(.late,
							needCoffe: 0.05,
							needMilk: 0.2,
							needSugar: 0.04,
							needWater: 0.1)
		}
		return result
	}
}
