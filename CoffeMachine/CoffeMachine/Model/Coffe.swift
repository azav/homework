//
//  Coffe.swift
//  CoffeMachine
//
//  Created by Anton Zavgorodniy on 4/24/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import Foundation
enum Coffe {
	case americano
	case espresso
	case late
	case capuchino
}
