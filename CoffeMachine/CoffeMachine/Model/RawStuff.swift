//
//  RawStuff.swift
//  CoffeMachine
//
//  Created by Anton Zavgorodniy on 4/24/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import Foundation
enum RawStuff {
	case coffe
	case water
	case milk
	case sugar
}
