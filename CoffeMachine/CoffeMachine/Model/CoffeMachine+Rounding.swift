//
//  CoffeMachine+Rounding.swift
//  CoffeMachine
//
//  Created by Anton Zavgorodniy on 4/24/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import Foundation

extension CoffeMachine {
	// Метод для округления свойств типа дабл перед выводом на экран
	func cut(_ cut: Double) -> Double {
		return  Double(round(100*cut)/100)
	}
}
